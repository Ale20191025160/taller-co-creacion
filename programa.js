class calculadora{
    //atributos
    num1=5;
    num2=3;
    resultado
    //metodos
    sumar(){
        this.resultado= this.num1+this.num2
        console.log(this.resultado)
    }
    restar(){
        this.resultado= this.num1-this.num2
        console.log(this.resultado)
    }
    multiplicar(){
        this.resultado= this.num1*this.num2
        console.log(this.resultado)
    }
    dividir(){
        this.resultado= this.num1/this.num2
        console.log(this.resultado)
    }
    modulo(){
        this.resultado= this.num1%this.num2
        console.log(this.resultado)
    }
}
class calculadoraCientifica extends calculadora{
    calcularSeno(){
        this.resultado= Math.sin(this.num1)
        console.log(this.resultado)
    }
    calcularCoseno(){
        this.resultado= Math.cos(this.num1)
        console.log(this.resultado)
    }
    calcularTangente(){
        this.resultado= Math.tan(this.num1)
        console.log(this.resultado)
    }
}
class calculadoraConversora extends calculadoraCientifica{
    gradosAradianes(){
        this.resultado= this.num1*(Math.PI/180)
        console.log(this.resultado)
    }
    radianesAgrados(){
        this.resultado= this.num1*(180/Math.PI)
        console.log(this.resultado)
    }
    kelvinAcentigrados(){
        this.resultado= this.num1-273.15
        console.log(this.resultado)
    }
    centigradosAkelvin(){
        this.resultado= this.num1+273.15
        console.log(this.resultado)
    }
}
let micalculadoracientifica=new calculadoraConversora();